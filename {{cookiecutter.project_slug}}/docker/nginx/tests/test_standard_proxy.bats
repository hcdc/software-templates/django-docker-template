# test script for standard proxy configurations


setup() {
    load "test_helper/common-setup"
    _common_setup
}

@test "Test for X-Forwarded-Proto Header" {
    # make sure that something like the following is defined in a location with
    # proxy_pass:
    #
    # proxy_set_header X-Forwarded-Proto $forwarded_proto;
    #
    # where $forwarded_proto is defined via the nginx-cfg/proto_map.conf

    # get all location parameters and first strip comments
    ALL_LOCATIONS="$(grep -vnr --include='*.conf' "^\s*[#\;]\|^\s*$" /etc/nginx/ /opt/app-root/etc  | sed -n '/location .* {/,/}/p')"

    while [[ "$ALL_LOCATIONS" =~ "location" ]]; do

      # extract content within brackets
      LOCATION=${ALL_LOCATIONS#*'{'}
      LOCATION=${LOCATION%%'}'*}


      # strip this location section
      ALL_LOCATIONS=${ALL_LOCATIONS#*'}'}

      if [[ "$LOCATION" =~ "proxy_pass" ]]; then
        assert_regex "$LOCATION" 'proxy_set_header\s+X-Forwarded-Proto\s+\$forwarded_proto\s*;'
      fi
    done
}

@test "Test for X-Forwarded-Host Header" {
    # make sure that something like the following is defined in a location with
    # proxy_pass:
    #
    # proxy_set_header X-Forwarded-Host $host;

    # get all location parameters and first strip comments
    ALL_LOCATIONS="$(grep -vnr --include='*.conf' "^\s*[#\;]\|^\s*$" /etc/nginx/ /opt/app-root/etc  | sed -n '/location .* {/,/}/p')"

    while [[ "$ALL_LOCATIONS" =~ "location" ]]; do

      # extract content within brackets
      LOCATION=${ALL_LOCATIONS#*'{'}
      LOCATION=${LOCATION%%'}'*}


      # strip this location section
      ALL_LOCATIONS=${ALL_LOCATIONS#*'}'}

      if [[ "$LOCATION" =~ "proxy_pass" ]]; then
        assert_regex "$LOCATION" 'proxy_set_header\s+X-Forwarded-Host\s+\$(http_)?host\s*;'
      fi
    done
}

@test "Test for Host Header" {
    # make sure that something like the following is defined in a location with
    # proxy_pass:
    #
    # proxy_set_header Host $http_host;

    # get all location parameters and first strip comments
    ALL_LOCATIONS="$(grep -vnr --include='*.conf' "^\s*[#\;]\|^\s*$" /etc/nginx/ /opt/app-root/etc  | sed -n '/location .* {/,/}/p')"

    while [[ "$ALL_LOCATIONS" =~ "location" ]]; do

      # extract content within brackets
      LOCATION=${ALL_LOCATIONS#*'{'}
      LOCATION=${LOCATION%%'}'*}


      # strip this location section
      ALL_LOCATIONS=${ALL_LOCATIONS#*'}'}

      if [[ "$LOCATION" =~ "proxy_pass" ]]; then
        assert_regex "$LOCATION" 'proxy_set_header\s+Host\s+\$(http_)?host\s*;'
      fi
    done
}

@test "Test for proxy_redirect off" {
    # make sure that something like the following is defined in a location with
    # proxy_pass:
    #
    # proxy_redirect off;

    # get all location parameters and first strip comments
    ALL_LOCATIONS="$(grep -vnr --include='*.conf' "^\s*[#\;]\|^\s*$" /etc/nginx/ /opt/app-root/etc  | sed -n '/location .* {/,/}/p')"

    while [[ "$ALL_LOCATIONS" =~ "location" ]]; do

      # extract content within brackets
      LOCATION=${ALL_LOCATIONS#*'{'}
      LOCATION=${LOCATION%%'}'*}


      # strip this location section
      ALL_LOCATIONS=${ALL_LOCATIONS#*'}'}

      if [[ "$LOCATION" =~ "proxy_pass" ]]; then
        assert_regex "$LOCATION" 'proxy_redirect\s+off\s*;'
      fi
    done
}
